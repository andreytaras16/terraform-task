
module "vpc" {
  source = "./modules/vpc"
}

module "sgs" {
  source = "./modules/sgs"
  vpc_id = module.vpc.vpc_id
  vpc_cidr_block = module.vpc.vpc_cidr_block
}

module "gitlab-inst" {
  source = "./modules/gitlab-inst"
  public_subnet_ids = module.vpc.public_subnet_ids
  vpc_id = module.vpc.vpc_id
  security_group_ids = tolist([module.sgs.db_security_group_id, module.sgs.http_ssh_security_group_id])
}

module "db" {
  source = "./modules/db"
  private_subnet_ids = module.vpc.private_subnet_ids
  security_group_ids = tolist([module.sgs.db_security_group_id])
}