module "db_default" {
  source  = "terraform-aws-modules/rds/aws"
  version = "3.1.0"

  identifier = "django-postgre-db"

  create_db_option_group    = false
  create_db_parameter_group = false

  engine               = "postgres"
  engine_version       = "11.10"
  family               = "postgres11" # DB parameter group
  major_engine_version = "11"         # DB option group
  instance_class       = "db.t2.micro"

  allocated_storage = 20

  name                   = "djangodb"
  username               = "andrey"
  create_random_password = false
  password               = "123456789"
  port                   = 5432

  subnet_ids             = var.private_subnet_ids
  vpc_security_group_ids = var.security_group_ids

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  backup_retention_period = 0

}