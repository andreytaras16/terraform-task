resource "aws_key_pair" "terraform-demo" {
  key_name   = "terraform-demo"
  public_key = "${file("/home/kpkq/.ssh/id_rsa.pub")}"
}

resource "aws_instance" "my-instance" {
	ami = "ami-0d5eff06f840b45e9"
	instance_type = "t2.micro"
	subnet_id = var.public_subnet_ids[0]
	key_name = "${aws_key_pair.terraform-demo.key_name}"
	user_data = "${file("install.sh")}"

  security_groups = var.security_group_ids
	tags = {
		Name = "Terraform"	
		Batch = "5AM"
	}
}
