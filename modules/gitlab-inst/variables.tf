variable "public_subnet_ids" {
  description = "Public subnet ids"
  type        = list
}

variable "vpc_id" {
  description = "VPC's id"
  type        = string
}

variable "security_group_ids" {
  description = "Security group ids"
  type        = list
}
