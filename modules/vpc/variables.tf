variable "availability_zones" {
  type        = list(string)
  description = "List of Availability Zones where subnets will be created"
  default = ["us-east-1a", "us-east-1b"]
}