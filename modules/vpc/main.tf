module "vpc" {
  source  = "cloudposse/vpc/aws"
  cidr_block = "172.16.0.0/16"
  namespace  = "tf"
  stage      = "test"
  name       = "django"
}

module "subnets" {
  source  = "cloudposse/dynamic-subnets/aws"
  availability_zones       = var.availability_zones
  vpc_id                   = module.vpc.vpc_id
  igw_id                   = module.vpc.igw_id
  cidr_block               = module.vpc.vpc_cidr_block
  nat_gateway_enabled      = false
  nat_instance_enabled     = true
  nat_instance_type        = "t2.micro"
}
