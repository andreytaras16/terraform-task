variable "vpc_id" {
  description = "VPC's id"
  type        = string
}

variable "vpc_cidr_block" {
  description = "VPC's cidr block"
  type        = string
}