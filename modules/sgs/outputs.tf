output "http_ssh_security_group_id" {
  value       = module.http_ssh_sg.security_group_id
  description = "The security group id of http+ssh connection"
}

output "db_security_group_id" {
  value       = module.security_group.security_group_id
  description = "The security group id of database"
}