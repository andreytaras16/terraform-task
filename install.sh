#!/bin/bash

sleep 30
# install docker
sudo yum update
sudo yum install -y docker
sudo usermod -a -G docker ec2-user
sudo service docker start
sudo chkconfig docker on

# install docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version

# install 
sudo yum -y install git

# clone project 
git clone https://gitlab.com/andreytaras16/lesson2-homework.git
cd lesson2-homework/web_app
sudo pip3 install -r requirements.txt
sleep 300
python3 manage.py makemigrations
python3 manage.py migrate
echo Migrated


sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo yum -y install gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
gitlab-runner register --url=https://gitlab.com/ \
 --registration-token=69d_SmCs_YULqiLrah8J \
 --non-interactive=true --locked=false \
 --name=gitlab-runner --executor=shell \
 --tag-list=terraform
gitlab-runner run
